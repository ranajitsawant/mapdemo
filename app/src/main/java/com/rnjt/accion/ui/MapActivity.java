package com.rnjt.accion.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rnjt.accion.R;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    Double lat = 19.8777778, lon = 72.6656568;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        findViewById(R.id.textBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            lat =getIntent().getDoubleExtra("lat", 0.0);
        } catch (Exception e) {
            lat = 19.8777778;
        }
        try {
            lon = getIntent().getDoubleExtra("lon", 0.0);
        } catch (Exception e) {
            lon = 72.6656568;
        }
        try {
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lon))
                .title(getIntent().getStringExtra("country")));

        LatLng loca = new LatLng(lat, lon);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loca, 15));
    }


}
