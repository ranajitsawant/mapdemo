package com.rnjt.accion.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.rnjt.accion.R;
import com.rnjt.accion.adapter.CountryListAdapter;
import com.rnjt.accion.api.model.Country;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, CountryListAdapter.OnItemClickListener, MaterialSearchBar.OnSearchActionListener {

    CountryListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    public static ArrayList<Country> data;
    private MaterialSearchBar searchBar;

    Double lat = 19.8777778, lon = 72.6656568;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.setHint("Search Country");
        searchBar.setSpeechMode(false);
        searchBar.setOnSearchActionListener(this);
        searchBar.hideSuggestionsList();
        searchBar.setLastSuggestions(new ArrayList());
        searchBar.setPlaceHolder("Search Country");


        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<Country>();
         data = getData();

        if (data != null && data.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new CountryListAdapter(data, this);
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setVisibility(View.GONE);
        }


    }

    @Override
    public void onItemClick(Country country) {

        if (getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT) {
            startActivity(new Intent(this, MapActivity.class).putExtra("lat", country.getCoord().getLat()).putExtra("lon", country.getCoord().getLon()));
        } else {
            try {
                lat = country.getCoord().getLat();
            } catch (Exception e) {
                lat = 19.8777778;
            }
            try {
                lon = country.getCoord().getLon();
            } catch (Exception e) {
                lon = 72.6656568;
            }
            try {
                mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();

            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }


    void filter(String text) {
        ArrayList<Country> temp = new ArrayList();
        if (data != null && data.size() > 0) {
            for (Country d : data) {
                if (d.getName().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }
            adapter.updateList(temp);
        }
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {
        if (!enabled)
            adapter.updateList(data);
    }

    @Override
    public void onSearchConfirmed(CharSequence text) {
        filter(text.toString());
    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lon))
                .title(getIntent().getStringExtra("country")));

        LatLng loca = new LatLng(lat, lon);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loca, 15));
    }


    protected int getScreenOrientation() {
        Display getOrient = getWindowManager().getDefaultDisplay();
        Point size = new Point();

        getOrient.getSize(size);

        int orientation;
        if (size.x < size.y) {
            orientation = Configuration.ORIENTATION_PORTRAIT;
        } else {
            orientation = Configuration.ORIENTATION_LANDSCAPE;
        }
        return orientation;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("citi_lite.json");
            // InputStream is = getAssets().open("cities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public ArrayList<Country> getData() {


        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(loadJSONFromAsset());

            for (int i = 0; i < jsonarray.length(); i++) {
                Country country = new Gson().fromJson(String.valueOf(jsonarray.getJSONObject(i)), Country.class);
                data.add(country);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }
}


