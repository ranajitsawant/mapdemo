package com.rnjt.accion.adapter;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rnjt.accion.R;
import com.rnjt.accion.api.model.Country;

import java.util.ArrayList;

public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.MyViewHolder> {

    private ArrayList<Country> dataSet;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(Country collections);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textName = holder.textName;

        textName.setText("" + dataSet.get(listPosition).getName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (listPosition % 2 == 0) {
                textName.setBackground(ContextCompat.getDrawable(textName.getContext(), R.color.gray));
            } else {
                textName.setBackground(ContextCompat.getDrawable(textName.getContext(), R.color.white));
            }
        }
        try {
            holder.layItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(dataSet.get(listPosition));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CountryListAdapter(ArrayList<Country> data, OnItemClickListener listener) {
        mListener = listener;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_country, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textName;
        LinearLayout layItem;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textName = itemView.findViewById(R.id.textName);
            this.layItem = itemView.findViewById(R.id.layItem);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
    public void updateList(ArrayList<Country> list){
        dataSet = list;
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}